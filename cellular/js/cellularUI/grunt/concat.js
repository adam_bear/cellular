module.exports = {
    options: {
        separator: '\n \n ///// \n',
    },
    scripts: {
        src: [
            'js/jquery.init.js',
            'js/drupal.init.js',
            'js/cellular.init.js',
            'js/cellular.functions.js',
            'js/cellular.jAccordion.js',
            'js/cellular.jBlocklink.js',
            //'js/cellular.jCarousel.js',
            //'js/cellular.jEqualheight.js',
            'js/cellular.jFormal.js',
            'js//js/cellular.jMmenujs',
            //'js/cellular.jParallax.js',
            'js/cellular.jScrolli.js',
            //'js/cellular.jScrolltrigger.js',
            //'js/cellular.jSticky.js',
            'js/cellular.jTabs.js',
            'js/cellular.end.js',
            'js/drupal.end.js',
            'js/jquery.end.js',
        ],
        dest: 'jquery.cellular.js',
    },
};