module.exports = {
    options: {
        separator: '\n \n ///// :) \n',
    },
    cellularUI: {
        src: [
            'js/cellularUI/js/jquery.init.js',
            'js/cellularUI/js/drupal.init.js',
            'js/cellularUI/js/cellular.init.js',
            'js/cellularUI/js/cellular.functions.js',
            'js/cellularUI/js/cellular.jAccordion.js',
            'js/cellularUI/js/cellular.jBlocklink.js',
            //'js/cellularUI/js/cellular.jCarousel.js',
            //'js/cellularUI/js/cellular.jEqualheight.js',
            'js/cellularUI/js/cellular.jFormal.js',
            'js/cellularUI/js/cellular.jMmenu.js',
            //'js/cellularUI/js/cellular.jParallax.js',
            'js/cellularUI/js/cellular.jScrolli.js',
            //'js/cellularUI/js/cellular.jScrolltrigger.js',
            //'js/cellularUI/js/cellular.jSticky.js',
            'js/cellularUI/js/cellular.jTabs.js',
            'js/cellularUI/js/cellular.end.js',
            'js/cellularUI/js/drupal.end.js',
            'js/cellularUI/js/jquery.end.js',
        ],
        dest: 'js/plugins/jquery.cellular.js',
    },
};